package com.bobby.week4_plusgame

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.bobby.week4_plusgame.databinding.FragmentPlayBinding


class PlayFragment : Fragment() {
    var correct: Int = 0
    var incorrect: Int = 0
    var isSelectAnswer = false
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentPlayBinding>(inflater,
            R.layout.fragment_play,container,false)
        binding.game = this

        fun showLose(btn: Button) {
            binding.apply {
                if(!isSelectAnswer){
                    btn.setBackgroundColor(Color.RED)
                    txtAnswer.text = "You're wrong"
                    incorrect++
                    txtStatic.text = "correct ${correct}\nincorrect ${incorrect}"
                    btnPlay.visibility = View.VISIBLE
                    isSelectAnswer =  true
                }
            }

        }
        fun showWin(btn: Button) {
            binding.apply {
                if(!isSelectAnswer){
                    btn.setBackgroundColor(Color.GREEN)
                    txtAnswer.text = "You're right"
                    correct++
                    txtStatic.text = "correct ${correct}\nincorrect ${incorrect}"
                    btnPlay.visibility = View.VISIBLE
                    isSelectAnswer =  true
                }
            }
        }

        fun play() {
            binding.apply {
                txtAnswer.text = ""
                btnPlay.visibility = View.GONE
                btn1.setBackgroundResource(android.R.drawable.btn_default);
                btn2.setBackgroundResource(android.R.drawable.btn_default);
                btn3.setBackgroundResource(android.R.drawable.btn_default);

                isSelectAnswer = false

                txtNum1.text = (1..10).random().toString()
                txtNum2.text = (1..10).random().toString()

                val randomBtn = (1..3).random().toString()
                val num1 = (txtNum1).text.toString().toInt()
                val num2 = (txtNum2).text.toString().toInt()

                var randomNum1: String
                var randomNum2: String
                var randomNum3: String

                randomNum1 = ((num1 +num2).toString())
                do {
                    randomNum2 = (1..20).random().toString()
                } while (randomNum2 == (num1 + num2).toString())
                do {
                    randomNum3 = (1..20).random().toString()
                } while (randomNum3 == (num1 + num2).toString() || randomNum3 == randomNum2)

                when (randomBtn) {
                    "1" -> {
                        btn1.text = (randomNum1.toInt()).toString()
                        btn2.text = (randomNum2.toInt()).toString()
                        btn3.text = (randomNum3.toInt()).toString()
                    }
                    "2" -> {
                        btn2.text = (randomNum1.toInt()).toString()
                        btn1.text = (randomNum2.toInt()).toString()
                        btn3.text = (randomNum3.toInt()).toString()
                    }
                    else -> {
                        btn3.text = (randomNum1.toInt()).toString()
                        btn1.text = (randomNum2.toInt()).toString()
                        btn2.text = (randomNum3.toInt()).toString()
                    }
                }

                btn1.setOnClickListener {
                    if (randomBtn == "1") {
                        showWin(btn1)
                    } else {
                        showLose(btn1)
                    }
                }
                btn2.setOnClickListener {
                    if (randomBtn == "2") {
                        showWin(btn2)
                    } else {
                        showLose(btn2)
                    }
                }
                btn3.setOnClickListener {
                    if (randomBtn == "3") {
                        showWin(btn3)
                    } else {
                        showLose(btn3)
                    }
                }
                btnPlay.setOnClickListener {
                    play()
                }
                btnBackMenu.setOnClickListener { view ->
                    view.findNavController()
                        .navigate(R.id.action_playFragment_to_menuFragment)
                }

            }
        }
        play()
        return binding.root
    }



}