package com.bobby.week4_plusgame

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.bobby.week4_plusgame.databinding.FragmentMenuBinding


class MenuFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentMenuBinding>(inflater,
            R.layout.fragment_menu,container,false)
        binding.menu = this

        binding.apply {
            btnPlusGame.setOnClickListener { view ->
                view.findNavController()
                    .navigate(R.id.action_menuFragment_to_playFragment)
            }
            btnMinusGame.setOnClickListener { view ->
                view.findNavController()
                    .navigate(R.id.action_menuFragment_to_playMinusFragment)
            }
            btnMultiplyGame.setOnClickListener { view ->
                view.findNavController()
                    .navigate(R.id.action_menuFragment_to_playMultiplyFragment)
            }
        }
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.option_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return view?.findNavController()?.let {
            NavigationUI.onNavDestinationSelected(item!!,
                it
            )
        }!! || super.onOptionsItemSelected(item)
    }
}